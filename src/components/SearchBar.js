import './SearchBar.css';
import React from 'react';

class SearchBar extends React.Component {
    state = { term: '' };

    onSearchSubmit = event => {
        event.preventDefault();

        this.props.onFormSubmit(this.state.term);
    }

    render() {
        return (
            <div className="search-bar ui container">
                <form className="ui form segment" onSubmit={this.onSearchSubmit}>
                    <div className="field">
                        <label>Search video</label>
                        <input
                            type="text"
                            value={this.state.term}
                            onChange={event => this.setState({ term: event.target.value })}
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchBar;