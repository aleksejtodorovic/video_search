import axios from 'axios';

const API_KEY = 'AIzaSyD0VoM36P7AnQyd1JnqIuf6d3puvOaXfAg';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        maxResults: 5,
        key: API_KEY,
        part: 'snippet'
    }
});